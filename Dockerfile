FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install -y \
    gcc \
    g++ \
    build-essential \
	git \
    libpthread-stubs0-dev \
    zlib1g-dev \
    python3.8 \
    python3-pip \
    && git clone https://bitbucket.org/jspagnuolo/trust4.git trust4 \
    && cd trust4 \
	&& make \
	&& cd .. 
ENV PATH /trust4:$PATH
RUN run-trust4 -b ./trust4/example/example.bam -f ./trust4/hg38_bcrtcr.fa --ref ./trust4/human_IMGT+C.fa \
